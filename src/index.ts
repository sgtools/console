import * as readline from 'readline';
import { stdin, stdout } from 'process';
import { Console } from 'console';

let mconsole = new Console(stdout);

const codes = {
    b           : [1, 22],
    bold        : [1, 22],
    i           : [3, 23],
    italic      : [3, 23],
    u           : [4, 24],
    underline   : [4, 24],
    inverse     : [7, 27],
    strike      : [9, 29],

    black       : [30, 39],
    r           : [31, 39],
    red         : [31, 39],
    g           : [32, 39],
    green       : [32, 39],
    y           : [33, 39],
    yellow      : [33, 39],
    bl          : [34, 39],
    blue        : [34, 39],
    white       : [37, 39],
    grey        : [90, 39]   
}

export class AppConsole
{
    privateColors: any;

    constructor()
    {
        this.privateColors = {};
    }

    /**
     * Add a private balise with color code
     * @param code Balise
     * @param start Color code number
     * @param end Color code 'reset' number
     */
    public addPrivateColor(code: string, start: number | string, end: number | string)
    {
        this.privateColors[code] = [ start, end ];
    }

    private removeStylesByList(message: string, colors: any)
    {
        Object.keys(colors).forEach(key => {
            let rex = `\\[${key}\\](.+?)\\[\\/${key}\\]`;
            let regex = new RegExp(rex, 'gi');
            let matches = Array.from(message.matchAll(regex));
            matches.forEach(match => {
                let oldString = match[0];
                let content = match[1];
                let newString = content;
                message = message.split(oldString).join(newString);
            });
        });
        return message;
    }

    private convertByList(message: string, colors: any)
    {
        Object.keys(colors).forEach(key => {
            let rex = `\\[${key}\\](.+?)\\[\\/${key}\\]`;
            let regex = new RegExp(rex, 'gi');
            let matches = Array.from(message.matchAll(regex));
            matches.forEach(match => {
                let oldString = match[0];
                let content = match[1];
                let start = '\u001b[' + colors[key][0] + 'm';
                let end = '\u001b[' + colors[key][1] + 'm';
                let newString = start + content + end;
                message = message.split(oldString).join(newString);
            });
        });
        return message;
    }

    public removeStyles(message: string)
    {
        message = this.removeStylesByList(message, codes);
        message = this.removeStylesByList(message, this.privateColors);
        return message;
    }

    public convert(message: string)
    {
        message = this.convertByList(message, codes);
        message = this.convertByList(message, this.privateColors);
        return message;
    }

    public print(message: string)
    {
        mconsole.log(this.convert(message));
    }

    public printLine(message: string)
    {
        let width = stdout.columns;
        let raw = this.removeStyles(message);
        if (raw.length > width) {
            message = message.substr(0, width - 3) + '...';
        } else {
            let len = width - raw.length;
            let blank = ''.padEnd(len);
            message = message.replace('$$', blank);
        }
        this.print(message);
    }

    public printList(items: string[])
    {
        items.forEach((item: string, index: number) => {
            console.print(`[b][y]‣  ${(index + 1).toString().padStart(2, '0')}[/y][/b]...${item}`);
        });
        console.newline();
    }

    public question(messages: string | string[]): Promise<string | string[]>
    {
        return new Promise((resolve, reject) => {
            let main = (function*() {
                let q = readline.createInterface({
                    input: stdin,
                    output: stdout
                });
                let results = [];
                let items = typeof messages === 'string' ? new Array(messages) : messages;

                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    let result = yield q.question(console.convert(`[i][y]${item} : [/y][/i]`), data => main.next(data));
                    results.push(result.toString());
                }
    
                q.close();
                resolve(results.length === 1 ? results[0] as string : results as string[]);
            })();    
            main.next();
        });
    }

    public clear()
    {
        mconsole.clear();
    }

    public newline()
    {
        mconsole.log('');
    }

    public log(data: any)
    {
        mconsole.log(data);
    }

    public success(message: string)
    {
        this.print(`[b][g]‣  ${message}[/g][/b]`);
    }

    public warn(message: string)
    {
        this.print(`[b][y]‣  ${message}[/y][/b]`);
    }

    public error(message: string)
    {
        this.print(`[r]‣  ${message}[/r]`);
    }

    public info(message: string)
    {
        this.print(`[b][grey]${message}[/grey][/b]`);
    }

}

export const console = new AppConsole();

