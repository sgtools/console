[@sgtools/console](README.md) / Exports

# @sgtools/console

## Table of contents

### Classes

- [AppConsole](classes/appconsole.md)

### Variables

- [console](modules.md#console)

## Variables

### console

• `Const` **console**: [*AppConsole*](classes/appconsole.md)

Defined in: index.ts:186
