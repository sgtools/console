@sgtools/console / [Exports](modules.md)

# Welcome to @sgtools/console
[![Version](https://npm.bmel.fr/-/badge/sgtools/console.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/console)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/console/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Console for nodejs applications

## Install

```sh
npm install @sgtools/console
```

## Usage

```sh
import { console } from '@sgtools/console';
```

Code documentation can be found [here](https://gitlab.com/sgtools/console/-/blob/master/docs/README.md).

## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>

## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/console/issues). You can also contact the author.

## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.
