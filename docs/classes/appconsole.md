[@sgtools/console](../README.md) / [Exports](../modules.md) / AppConsole

# Class: AppConsole

## Hierarchy

* **AppConsole**

## Table of contents

### Constructors

- [constructor](appconsole.md#constructor)

### Properties

- [privateColors](appconsole.md#privatecolors)

### Methods

- [addPrivateColor](appconsole.md#addprivatecolor)
- [clear](appconsole.md#clear)
- [convert](appconsole.md#convert)
- [convertByList](appconsole.md#convertbylist)
- [error](appconsole.md#error)
- [info](appconsole.md#info)
- [log](appconsole.md#log)
- [newline](appconsole.md#newline)
- [print](appconsole.md#print)
- [printLine](appconsole.md#printline)
- [printList](appconsole.md#printlist)
- [question](appconsole.md#question)
- [removeStyles](appconsole.md#removestyles)
- [removeStylesByList](appconsole.md#removestylesbylist)
- [success](appconsole.md#success)
- [warn](appconsole.md#warn)

## Constructors

### constructor

\+ **new AppConsole**(): [*AppConsole*](appconsole.md)

**Returns:** [*AppConsole*](appconsole.md)

Defined in: index.ts:32

## Properties

### privateColors

• **privateColors**: *any*

Defined in: index.ts:32

## Methods

### addPrivateColor

▸ **addPrivateColor**(`code`: *string*, `start`: *string* \| *number*, `end`: *string* \| *number*): *void*

Add a private balise with color code

#### Parameters:

Name | Type | Description |
------ | ------ | ------ |
`code` | *string* | Balise   |
`start` | *string* \| *number* | Color code number   |
`end` | *string* \| *number* | Color code 'reset' number    |

**Returns:** *void*

Defined in: index.ts:45

___

### clear

▸ **clear**(): *void*

**Returns:** *void*

Defined in: index.ts:149

___

### convert

▸ **convert**(`message`: *string*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *string*

Defined in: index.ts:91

___

### convertByList

▸ `Private`**convertByList**(`message`: *string*, `colors`: *any*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |
`colors` | *any* |

**Returns:** *string*

Defined in: index.ts:66

___

### error

▸ **error**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:174

___

### info

▸ **info**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:179

___

### log

▸ **log**(`data`: *any*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`data` | *any* |

**Returns:** *void*

Defined in: index.ts:159

___

### newline

▸ **newline**(): *void*

**Returns:** *void*

Defined in: index.ts:154

___

### print

▸ **print**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:98

___

### printLine

▸ **printLine**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:103

___

### printList

▸ **printList**(`items`: *string*[]): *void*

#### Parameters:

Name | Type |
------ | ------ |
`items` | *string*[] |

**Returns:** *void*

Defined in: index.ts:117

___

### question

▸ **question**(`messages`: *string* \| *string*[]): *Promise*<*string* \| *string*[]\>

#### Parameters:

Name | Type |
------ | ------ |
`messages` | *string* \| *string*[] |

**Returns:** *Promise*<*string* \| *string*[]\>

Defined in: index.ts:125

___

### removeStyles

▸ **removeStyles**(`message`: *string*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *string*

Defined in: index.ts:84

___

### removeStylesByList

▸ `Private`**removeStylesByList**(`message`: *string*, `colors`: *any*): *string*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |
`colors` | *any* |

**Returns:** *string*

Defined in: index.ts:50

___

### success

▸ **success**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:164

___

### warn

▸ **warn**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:169
