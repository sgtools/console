"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.console = exports.AppConsole = void 0;
const readline = require("readline");
const process_1 = require("process");
const console_1 = require("console");
let mconsole = new console_1.Console(process_1.stdout);
const codes = {
    b: [1, 22],
    bold: [1, 22],
    i: [3, 23],
    italic: [3, 23],
    u: [4, 24],
    underline: [4, 24],
    inverse: [7, 27],
    strike: [9, 29],
    black: [30, 39],
    r: [31, 39],
    red: [31, 39],
    g: [32, 39],
    green: [32, 39],
    y: [33, 39],
    yellow: [33, 39],
    bl: [34, 39],
    blue: [34, 39],
    white: [37, 39],
    grey: [90, 39]
};
class AppConsole {
    constructor() {
        this.privateColors = {};
    }
    /**
     * Add a private balise with color code
     * @param code Balise
     * @param start Color code number
     * @param end Color code 'reset' number
     */
    addPrivateColor(code, start, end) {
        this.privateColors[code] = [start, end];
    }
    removeStylesByList(message, colors) {
        Object.keys(colors).forEach(key => {
            let rex = `\\[${key}\\](.+?)\\[\\/${key}\\]`;
            let regex = new RegExp(rex, 'gi');
            let matches = Array.from(message.matchAll(regex));
            matches.forEach(match => {
                let oldString = match[0];
                let content = match[1];
                let newString = content;
                message = message.split(oldString).join(newString);
            });
        });
        return message;
    }
    convertByList(message, colors) {
        Object.keys(colors).forEach(key => {
            let rex = `\\[${key}\\](.+?)\\[\\/${key}\\]`;
            let regex = new RegExp(rex, 'gi');
            let matches = Array.from(message.matchAll(regex));
            matches.forEach(match => {
                let oldString = match[0];
                let content = match[1];
                let start = '\u001b[' + colors[key][0] + 'm';
                let end = '\u001b[' + colors[key][1] + 'm';
                let newString = start + content + end;
                message = message.split(oldString).join(newString);
            });
        });
        return message;
    }
    removeStyles(message) {
        message = this.removeStylesByList(message, codes);
        message = this.removeStylesByList(message, this.privateColors);
        return message;
    }
    convert(message) {
        message = this.convertByList(message, codes);
        message = this.convertByList(message, this.privateColors);
        return message;
    }
    print(message) {
        mconsole.log(this.convert(message));
    }
    printLine(message) {
        let width = process_1.stdout.columns;
        let raw = this.removeStyles(message);
        if (raw.length > width) {
            message = message.substr(0, width - 3) + '...';
        }
        else {
            let len = width - raw.length;
            let blank = ''.padEnd(len);
            message = message.replace('$$', blank);
        }
        this.print(message);
    }
    printList(items) {
        items.forEach((item, index) => {
            exports.console.print(`[b][y]‣  ${(index + 1).toString().padStart(2, '0')}[/y][/b]...${item}`);
        });
        exports.console.newline();
    }
    question(messages) {
        return new Promise((resolve, reject) => {
            let main = (function* () {
                let q = readline.createInterface({
                    input: process_1.stdin,
                    output: process_1.stdout
                });
                let results = [];
                let items = typeof messages === 'string' ? new Array(messages) : messages;
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    let result = yield q.question(exports.console.convert(`[i][y]${item} : [/y][/i]`), data => main.next(data));
                    results.push(result.toString());
                }
                q.close();
                resolve(results.length === 1 ? results[0] : results);
            })();
            main.next();
        });
    }
    clear() {
        mconsole.clear();
    }
    newline() {
        mconsole.log('');
    }
    log(data) {
        mconsole.log(data);
    }
    success(message) {
        this.print(`[b][g]‣  ${message}[/g][/b]`);
    }
    warn(message) {
        this.print(`[b][y]‣  ${message}[/y][/b]`);
    }
    error(message) {
        this.print(`[r]‣  ${message}[/r]`);
    }
    info(message) {
        this.print(`[b][grey]${message}[/grey][/b]`);
    }
}
exports.AppConsole = AppConsole;
exports.console = new AppConsole();
