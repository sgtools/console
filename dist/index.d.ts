export declare class AppConsole {
    privateColors: any;
    constructor();
    /**
     * Add a private balise with color code
     * @param code Balise
     * @param start Color code number
     * @param end Color code 'reset' number
     */
    addPrivateColor(code: string, start: number | string, end: number | string): void;
    private removeStylesByList;
    private convertByList;
    removeStyles(message: string): string;
    convert(message: string): string;
    print(message: string): void;
    printLine(message: string): void;
    printList(items: string[]): void;
    question(messages: string | string[]): Promise<string | string[]>;
    clear(): void;
    newline(): void;
    log(data: any): void;
    success(message: string): void;
    warn(message: string): void;
    error(message: string): void;
    info(message: string): void;
}
export declare const console: AppConsole;
